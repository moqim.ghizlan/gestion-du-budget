package com.example.myapplication;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Operation.class}, version = 1)
public abstract class OperationDatabase extends RoomDatabase {

    private static final String dbName = "operations";
    private static OperationDatabase operationDatabase;

    public static OperationDatabase getOperationDatabase(Context context) {
        if (operationDatabase == null) {
            operationDatabase = Room.databaseBuilder(context, OperationDatabase.class, dbName)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return operationDatabase;
    }

    public abstract OperationDao operationDao();
}
