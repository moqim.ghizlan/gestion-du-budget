package com.example.myapplication;

import android.app.Application;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.time.LocalDateTime;
import java.util.List;

public class OperationViewModel extends AndroidViewModel {

    private LiveData<List<Operation>> mAllOperations;
    private OperationRepository mRepository;
    private LiveData<Double> total;

    @RequiresApi(api = Build.VERSION_CODES.O)
    public OperationViewModel (Application application) {
        super(application);
        LocalDateTime now = LocalDateTime.now();
        int month = now.getMonth().getValue();
        int year = now.getYear();
        mRepository = new OperationRepository(application, month, year);
        mAllOperations = mRepository.getAllof();
        total = mRepository.getTotalLD();
    }
    LiveData<List<Operation>> getAllOf() { return mAllOperations; }

    LiveData<Double> getTotal(){ return this.total; }

    public void insert(Operation operation) { mRepository.insert(operation); }
}