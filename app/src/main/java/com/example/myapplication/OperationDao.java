package com.example.myapplication;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface OperationDao {

    @Insert
    void insert(Operation operation);

    @Query("SELECT * from operations where month=:month and year=:year ORDER BY day DESC")
    LiveData<List<Operation>> getAllOf(int month, int year);

    @Query("SELECT sum(amount) from operations where month=:month and year=:year")
    LiveData<Double> getSum(int month, int year);

}
