package com.example.myapplication;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class OperationRepository {

    private OperationDao mOperationDao;
    private LiveData<List<Operation>> mAllOperations;
    private LiveData<Double> totalLD;

    OperationRepository(Application application, int month, int year) {
        UserDatabase db = UserDatabase.getUserDatabase(application);
        mOperationDao = db.operationDao();
        mAllOperations = mOperationDao.getAllOf(month, year);
        totalLD = mOperationDao.getSum(month, year);

    }

    LiveData<List<Operation>> getAllof() {
        return mAllOperations;
    }

    LiveData<Double> getTotalLD() {
        return totalLD;
    }


    public void insert (Operation operation) {
        new insertAsyncTask(mOperationDao).execute(operation);
    }

    private static class insertAsyncTask extends AsyncTask<Operation, Void, Void> {

        private OperationDao mAsyncTaskDao;

        public insertAsyncTask(OperationDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Operation... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

}