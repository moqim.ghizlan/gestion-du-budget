package com.example.myapplication;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class Groupe extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_groupe, container, false);

        RecyclerView recyclerView = v.findViewById(R.id.groupesList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        List groupesList = new ArrayList<>();

        recyclerView.setAdapter(new GroupesAdapter(groupesList));



        return v;
    }

    public void AddGroupe(View view) {
    }
}



//public class Groupe {

//    private String name;

//    public Groupe(String name){
//        this.name = name;
//    }

//    public String getName() {
//        return name;
//    }
//}
