package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

public class homePage extends AppCompatActivity {

    TextView textViewName;

    BottomNavigationView bottomNavigationView;


    Groupe groupeFragment = new Groupe();
    Historique historiqueFragment = new Historique();
    Info info = new Info();
    Accueil accueil = new Accueil();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        String name = getIntent().getStringExtra("name");

        //bottom navigation view

        bottomNavigationView = findViewById(R.id.bottom_navigation);

        Bundle data = new Bundle();
        data.putString("name", name);
        accueil.setArguments(data);

        getSupportFragmentManager().beginTransaction().addToBackStack(null).add(R.id.container, accueil).commit();

        bottomNavigationView.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()){
                    case R.id.home:
                        getSupportFragmentManager().beginTransaction().addToBackStack(null).add(R.id.container, accueil).commit();
                        return true;
                    case R.id.groupe:
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, groupeFragment).commit();
                        return true;
                    case R.id.historique:
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, historiqueFragment).commit();
                        return true;
                    case R.id.info:
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, info).commit();
                        return true;
                }
                return false;
            }
        });
    }


}