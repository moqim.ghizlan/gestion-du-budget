package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class signupPage extends AppCompatActivity {

    EditText username, email, password, confirmPassword;
    MaterialButton register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_page);

        username = findViewById(R.id.usernameCreate);
        email = findViewById(R.id.emailCreate);
        password = findViewById(R.id.passwordCreate);
        confirmPassword = findViewById(R.id.confirmPasswordCreate);

        register = findViewById(R.id.createCompteBtn);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Creating user Entity
                UserEntity userEntity = new UserEntity();
                userEntity.setUsername(username.getText().toString());
                userEntity.setEmail(email.getText().toString());
                userEntity.setPassword(password.getText().toString());

                if(password.getText().toString().equals(confirmPassword.getText().toString())) {
                    if (validateInput(userEntity)) {
                        UserDatabase userDatabase = UserDatabase.getUserDatabase(getApplicationContext());
                        final UserDao userDao = userDatabase.userDao();

                        ExecutorService executorService = Executors.newSingleThreadExecutor();
                        Handler handler = new Handler(Looper.getMainLooper());
                        executorService.execute(() -> {
                            userDao.registerUser(userEntity);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Compte créé", Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(signupPage.this, loginPage.class);
                                    startActivity(i);
                                }
                            });
                        });
                    } else {
                        Toast.makeText(getApplicationContext(), "Saisissez tous les champs", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Confirmer votre mot de passe", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private Boolean validateInput(UserEntity userEntity){
        if(userEntity.getUsername().isEmpty() || userEntity.getEmail().isEmpty() || userEntity.getPassword().isEmpty()){
            return false;
        }else{
            return true;
        }
    }
}