package com.example.myapplication;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public class Historique extends Fragment {

    private OperationViewModel operationViewModel;

    private View v;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_historique, container, false);

        TextView viewTotal = v.findViewById(R.id.textViewTotal);


        EditText editTextDate = v.findViewById(R.id.editTextDate);
        LocalDateTime now = LocalDateTime.now();
        editTextDate.setText(String.valueOf(now.getDayOfMonth()));

        RecyclerView recyclerView = v.findViewById(R.id.OperationsList);
        OperationsAdapter adapter = new OperationsAdapter(this.getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));

        operationViewModel = new ViewModelProvider(this).get(OperationViewModel.class);

        operationViewModel.getAllOf().observe(this, new Observer<List<Operation>>() {
            @Override
            public void onChanged(List<Operation> operations) {
                adapter.setOperations(operations);
            }
        });

        operationViewModel.getTotal().observe(this, new Observer<Double>() {
            @Override
            public void onChanged(Double total) {
                viewTotal.setText(String.valueOf(operationViewModel.getTotal().getValue()));
            }
        });

        Button button = v.findViewById(R.id.buttonAddOperation);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText editTextName = v.findViewById(R.id.editTextTextPersonName);
                String name = editTextName.getText().toString();

                EditText editTextDate = v.findViewById(R.id.editTextDate);
                String date = editTextDate.getText().toString();

                EditText editTextAmount = v.findViewById(R.id.editTextNumberDecimal);
                String amount = editTextAmount.getText().toString();

                operationViewModel.insert(new Operation(name, Integer.parseInt(date),now.getMonth().getValue(), now.getYear(), Double.parseDouble(amount)));

                editTextName.setText("");
                editTextAmount.setText("");

            }
        });


        return v;
    }

}