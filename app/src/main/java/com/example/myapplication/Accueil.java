package com.example.myapplication;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.time.LocalDateTime;
import java.util.Date;


public class Accueil extends Fragment {

    private OperationViewModel operationViewModel;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_accueil, container, false);
        TextView tv = v.findViewById(R.id.nameHomePage);
        TextView da = v.findViewById(R.id.dateHomePage);

        Bundle data = getArguments();

        TextView viewTotal = v.findViewById(R.id.soldeHomePage);

        operationViewModel = new ViewModelProvider(this).get(OperationViewModel.class);

        operationViewModel.getTotal().observe(this, new Observer<Double>() {
            @Override
            public void onChanged(Double total) {
                viewTotal.setText(String.valueOf(operationViewModel.getTotal().getValue()));
            }
        });


        if(data!=null){
            String name = data.getString("name");
            tv.setText(name);
        }

        Date now = new Date();

        DateFormat dateformatter = DateFormat.getDateInstance(DateFormat.SHORT);
        String fromatteDate = dateformatter.format(now);
        da.setText(fromatteDate);

        return v;
    }
}