package com.example.myapplication;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.List;

public class OperationsAdapter extends RecyclerView.Adapter<OperationsAdapter.OperationViewHolder> {

    private final LayoutInflater mInflater;
    private List<Operation> operationsList;

    public OperationsAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public OperationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Create a new view, which defines the UI of the list item
        View itemView = mInflater.inflate(R.layout.operation_item, parent, false);
        return new OperationViewHolder(itemView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(OperationViewHolder holder, final int position) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        if (operationsList != null) {
            Operation operation = operationsList.get(position);
            holder.operationNameItemView.setText(operation.name);

            holder.operationDateItemView.setText(String.valueOf(operation.day) + "/" + String.valueOf(operation.month) + "/" + String.valueOf(operation.year));

            holder.operationAmountItemView.setText(String.valueOf(operation.amount) + " €");
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if (operationsList != null) {
            return operationsList.size();
        }
        return 0;
    }

    void setOperations(List<Operation> operations){
        operationsList = operations;
        notifyDataSetChanged();
    }

    class OperationViewHolder extends RecyclerView.ViewHolder {
        private final TextView operationNameItemView;
        private final TextView operationDateItemView;
        private final TextView operationAmountItemView;


        private OperationViewHolder(View itemView){
            super(itemView);
            operationNameItemView = itemView.findViewById(R.id.textViewOperationName);
            operationDateItemView = itemView.findViewById(R.id.textViewOperationDate);
            operationAmountItemView = itemView.findViewById(R.id.textViewOperationAmount);

        }
    }
}
