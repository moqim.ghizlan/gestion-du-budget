package com.example.myapplication;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface UserDao {
    //user
    @Insert
    void registerUser(UserEntity userEntity);

    @Query("SELECT * from users where username=(:username) and password = (:password)")
    UserEntity login(String username, String password);
}
